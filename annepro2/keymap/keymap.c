#include QMK_KEYBOARD_H

// Layers
typedef enum {
    _BASE_LAYER,
    // Enable by toggling
    _MOD_ARROWS,
    _WASD_ARROWS,
    _HJKL_ARROWS,
    // Enable by FN keys
    _FN1_LAYER,
    _FN2_LAYER,
    // _MOUSE1_LAYER,
    // _MOUSE2_LAYER,
    _TG_LAYER
} anne_pro_layers;

// Custom keycodes
enum {
    KM_SLP = SAFE_RANGE
};

enum {
    // Dictionary
    KA_DICT = QK_PROGRAMMABLE_BUTTON_1,
    // File Manager
    KA_FMAN = QK_PROGRAMMABLE_BUTTON_2,
    // Chat
    KA_CHAT = QK_PROGRAMMABLE_BUTTON_3,
    // Launcher
    KA_LACH = QK_PROGRAMMABLE_BUTTON_4,
    // Terminal
    KA_TERM = QK_PROGRAMMABLE_BUTTON_5,
    // Browser
    KA_BROW = QK_PROGRAMMABLE_BUTTON_6
};

const uint16_t keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    /*
     * Layer _BASE_LAYER
     * ,-----------------------------------------------------------------------------------------.
     * | GESC|  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |  9  |  0  |  -  |  =  |    Bksp   |
     * |-----------------------------------------------------------------------------------------+
     * | Tab    |  q  |  w  |  e  |  r  |  t  |  y  |  u  |  i  |  o  |  p  |  [  |  ]  |   \    |
     * |-----------------------------------------------------------------------------------------+
     * | FN1     |  a  |  s  |  d  |  f  |  g  |  h  |  j  |  k  |  l  |  ;  |  '  |    Enter    |
     * |-----------------------------------------------------------------------------------------+
     * | Shift      |  z  |  x  |  c  |  v  |  b  |  n  |  m  |  ,  |  .  |  /  |    Shift       |
     * |-----------------------------------------------------------------------------------------+
     * |  ALT  |  Ctrl  |  L1  |               space             |  TG   |  Alt  |  Ctrl  | FN2  |
     * \-----------------------------------------------------------------------------------------/
     *
     * Layer TAP in _BASE_LAYER (Not use currently)
     * ,-----------------------------------------------------------------------------------------.
     * |     |     |     |     |     |     |     |     |     |     |     |     |     |           |
     * |-----------------------------------------------------------------------------------------+
     * |        |     |     |     |     |     |     |     |     |     |     |     |     |        |
     * |-----------------------------------------------------------------------------------------+
     * |         |     |     |     |     |     |     |     |     |     |     |     |             |
     * |-----------------------------------------------------------------------------------------+
     * |            |     |     |     |     |     |     |     |     |     |     |                |
     * |-----------------------------------------------------------------------------------------+
     * |       |       |       |                                 |       |       |       |       |
     * \-----------------------------------------------------------------------------------------/
     */
    [_BASE_LAYER] = LAYOUT_60_ansi( /* Base */
        QK_GESC, KC_1, KC_2, KC_3, KC_4, KC_5, KC_6, KC_7, KC_8, KC_9, KC_0, KC_MINS, KC_EQL, KC_BSPC,
        KC_TAB,  KC_Q, KC_W, KC_E, KC_R, KC_T, KC_Y, KC_U, KC_I, KC_O, KC_P, KC_LBRC, KC_RBRC, KC_BSLS,
        MO(_FN1_LAYER), KC_A, KC_S, KC_D, KC_F, KC_G, KC_H, KC_J, KC_K, KC_L, KC_SCLN, KC_QUOT, KC_ENT,
        KC_LSFT, KC_Z, KC_X, KC_C, KC_V, KC_B, KC_N, KC_M, KC_COMM, KC_DOT, KC_SLSH, KC_RSFT,
        KC_LALT, KC_LCTL, KC_LGUI, KC_SPC, MO(_TG_LAYER), KC_RALT, KC_RCTL, MO(_FN2_LAYER)
    ),

    /*
     * Layer _FN1_LAYER
     * ,-----------------------------------------------------------------------------------------.
     * | ESC |  F1 |  F2 |  F3 |  F4 |  F5 |  F6 |  F7 |  F8 |  F9 | F10 | F11 | F12 |  DELETE   |
     * |-----------------------------------------------------------------------------------------+
     * |        |     | UP  |     |     |     |     |     |     |     | PS | HOME | END |  GRV   |
     * |-----------------------------------------------------------------------------------------+
     * |         |LEFT |DOWN |RIGHT|     |     |LEFT |DOWN | UP  |RIGHT| PGUP|PGDN |    PEnter   |
     * |-----------------------------------------------------------------------------------------+
     * |            |     | Cut | Copy|Paste|     |     |     |     |INSRT|FIND |                |
     * |-----------------------------------------------------------------------------------------+
     * |       |      |        |               Enter             |       |       |        |      |
     * \-----------------------------------------------------------------------------------------/
     *
     */
    [_FN1_LAYER] = LAYOUT_60_ansi(
        KC_ESC,  KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,   KC_F9,    KC_F10,  KC_F11,  KC_F12, KC_DEL,
        _______, _______, KC_UP,   _______, _______, _______, _______, _______, _______, _______,  KC_PSCR, KC_HOME, KC_END, KC_GRV,
        _______, KC_LEFT, KC_DOWN, KC_RGHT, _______, _______, KC_LEFT, KC_DOWN, KC_UP,   KC_RIGHT, KC_PGUP, KC_PGDN, KC_PENT,
        _______, _______, KC_CUT,  KC_COPY, KC_PSTE, _______, _______, _______, _______, KC_INS,   KC_FIND, _______,
        _______, _______, _______, KC_ENT,  _______, _______, _______, _______
    ),

    /*
     * Layer _FN2_LAYER
     * ,-----------------------------------------------------------------------------------------.
     * |      |MPLY |    |     |     |     |     |     |     |LEDON|VolM |VolD |VolU |           |
     * |-----------------------------------------------------------------------------------------+
     * |RecDelay|MREC |     |     |     |     |     |     |     |     |    | Next | Prev|  Play  |
     * |-----------------------------------------------------------------------------------------+
     * | Caps    |     |     |DICT |     |     |     |     |     |     |     |     |             |
     * |-----------------------------------------------------------------------------------------+
     * |            |     |     |Calc |     |     |     |Mail |     |     |     |                |
     * |-----------------------------------------------------------------------------------------+
     * |       |      |        |                                 |       |       |        |      | 
     * \-----------------------------------------------------------------------------------------/
     *
     */
    [_FN2_LAYER] = LAYOUT_60_ansi(
        _______, DM_PLY1, _______, _______, _______, _______, _______, _______, _______, KC_AP_RGB_TOG, KC_MUTE, KC_VOLD, KC_VOLU, _______,
        KM_SLP,  DM_REC1, _______, _______, KA_LACH, _______, _______, _______, _______, _______, _______, KC_MRWD, KC_MFFD, KC_MPLY,
        KC_CAPS, _______, _______, KA_DICT, KA_FMAN, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, KA_TERM, KA_CHAT, _______, KA_BROW, KC_CALC, KC_MAIL, _______, _______, _______, _______,
        _______, _______, _______, _______, _______, _______, _______, _______
    ),
    
    // Mouse 1 Layer 
    // HJKL -> Mouse movement
    // SPACE -> Left mouse btn
    // LGUI -> Right mouse btn
    // WASD -> Mouse wheel
    // [_MOUSE1_LAYER] = LAYOUT_60_ansi( /* Base */
    //     _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
    //     _______, _______, KC_WH_U, KC_BTN3, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
    //     MO(_MOUSE2_LAYER), KC_WH_L, KC_WH_D, KC_WH_R, _______, _______, KC_MS_L, KC_MS_D, KC_MS_U, KC_MS_R, _______, _______, _______,
    //     _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
    //     TG(_MOUSE1_LAYER), _______, _______, KC_BTN1, KC_BTN2, _______, _______, _______
    // ),

    // Mouse 2 Layer
    // 1. Swap mouse movement and mouse wheel
    // 2. Other key change to FN1 layout
    // [_MOUSE2_LAYER] = LAYOUT_60_ansi( /* Base */
    //      KC_ESC, KC_F1, KC_F2, KC_F3, KC_F4, KC_F5, KC_F6, KC_F7, KC_F8, KC_F9, KC_F10, KC_F11, KC_F12, KC_DEL,
    //      _______, _______, KC_MS_U, KC_E, _______, _______, _______, _______, _______, KC_BTN3, KC_PSCR, KC_HOME, KC_END,  _______,
    //      KC_CAPS, KC_MS_L, KC_MS_D, KC_MS_R, _______, _______, KC_WH_L, KC_WH_D, KC_WH_U, KC_WH_R, KC_PGUP, KC_PGDN, _______,
    //      _______, _______, _______, _______, _______, _______, _______, _______, _______, KC_INS,  KC_FIND, _______,
    //      TG(_MOUSE1_LAYER), _______, _______, _______, _______, _______, _______, _______
    // ),
    
    // Use right mod keys as arrow keys
    [_MOD_ARROWS] = LAYOUT_60_ansi( /* Base */
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, KC_UP,
        _______, _______, _______, _______, _______, KC_LEFT, KC_DOWN, KC_RIGHT
    ),

    // Use WASD as arrow keys
    [_WASD_ARROWS] = LAYOUT_60_ansi( /* Base */
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, KC_UP,   _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, KC_LEFT, KC_DOWN, KC_RIGHT, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, _______, _______, _______, _______, _______, _______
    ),
    
    // Use HJKL as arrow keys
    [_HJKL_ARROWS] = LAYOUT_60_ansi( /* Base */
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, _______, _______, _______, _______, KC_LEFT, KC_DOWN, KC_UP,  KC_RIGHT, _______, _______, _______,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, _______, _______, _______, _______, _______, _______
    ),

    /*
     * Layer _TG_LAYER
     *
     * Use to toggle layer instead of MO.
     * ,-----------------------------------------------------------------------------------------.
     * |QKBOOT|ARROWS|WASD |HJKL |     |     |     |     |     |     |     |     |     |         |
     * |-----------------------------------------------------------------------------------------+
     * |        |     |     |     |     |     |     |     |     |     |     |     |     |        |
     * |-----------------------------------------------------------------------------------------+
     * | FN1_TG  |     |     |     |     |     |     |     |     |     |     |     |             |
     * |-----------------------------------------------------------------------------------------+
     * |            |     |     |     |     |     |     |     |     |     |     |                |
     * |-----------------------------------------------------------------------------------------+
     * | FN2_TG|        |      |                                 |       |       |        |FN2_TG|
     * \-----------------------------------------------------------------------------------------/
     */
    [_TG_LAYER] = LAYOUT_60_ansi( /* Base */
        QK_BOOT,  TG(_MOD_ARROWS), TG(_WASD_ARROWS), TG(_HJKL_ARROWS), _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______,  _______, _______,   _______, _______, _______, _______, _______, _______, _______,  _______, _______, _______, _______,
        TG(_FN1_LAYER), _______, _______, _______, _______, _______, _______, _______, _______,   _______, _______, _______, _______,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
        _______, _______, _______, _______, TG(_FN2_LAYER), _______, _______, _______
    ),
};
const uint16_t keymaps_size = sizeof(keymaps);

// Super key state
bool super_key_pressed = false;

void matrix_init_user(void) {}

void matrix_scan_user(void) {}

// Code to run after initializing the keyboard
void keyboard_post_init_user(void) {
    // Use RGB Matrix
    rgb_matrix_enable_noeeprom();
    rgb_matrix_mode_noeeprom(RGB_MATRIX_SOLID_MULTISPLASH);
    rgb_matrix_sethsv_noeeprom(0x8C, 0xFF, 0xFF);
}

void set_layer_indicator(anne_pro_layers layer) {
    // Check super key first.
    if (super_key_pressed) {
        rgb_matrix_set_color_all(0x00, 0xFF, 0x00);
        return;
    }

    switch(layer) {
        case _FN1_LAYER:
            // Set the leds to green
            // ap2_led_set_foreground_color(0x00, 0xFF, 0x00);
            rgb_matrix_set_color_all(0x00, 0xFF, 0x00);

            break;
        case _FN2_LAYER:
            // Set the leds to blue
            rgb_matrix_set_color_all(0x00, 0x00, 0xFF);
            break;
        /*case _MOUSE1_LAYER:
        case _MOUSE2_LAYER:
            // Set the leds to white
            ap2_led_set_foreground_color(0xFF, 0xFF, 0xFF);
            break;*/
        case _TG_LAYER:
            rgb_matrix_set_color_all(0xFF, 0x00, 0x00);
            break;
        default:
            // Reset back to the current profile
            // ap2_led_reset_foreground_color();
            break;
    }
}

layer_state_t layer_state_set_user(layer_state_t state) {
    anne_pro_layers layer = get_highest_layer(state);

    const ap2_led_t color = {
        .p.red   = 0x00,
        .p.green = 0xFF,
        .p.blue  = 0x00,
        .p.alpha = 0xFF
    };
    
    // Special layer only lightup WASD
    if (IS_LAYER_ON_STATE(state, _WASD_ARROWS)) {
        ap2_led_sticky_set_key(1, 2, color);
        ap2_led_sticky_set_key(2, 1, color);
        ap2_led_sticky_set_key(2, 2, color);
        ap2_led_sticky_set_key(2, 3, color);
    } else {
        ap2_led_unset_sticky_key(1, 2);
        ap2_led_unset_sticky_key(2, 1);
        ap2_led_unset_sticky_key(2, 2);
        ap2_led_unset_sticky_key(2, 3);
    }

    // Special layer only lightup HJKL
    if (IS_LAYER_ON_STATE(state, _HJKL_ARROWS)) {
        ap2_led_sticky_set_key(2, 6, color);
        ap2_led_sticky_set_key(2, 7, color);
        ap2_led_sticky_set_key(2, 8, color);
        ap2_led_sticky_set_key(2, 9, color);
    } else {
        ap2_led_unset_sticky_key(2, 6);
        ap2_led_unset_sticky_key(2, 7);
        ap2_led_unset_sticky_key(2, 8);
        ap2_led_unset_sticky_key(2, 9);
    }

    // Special layer only lightup mod arrows
    if (IS_LAYER_ON_STATE(state, _MOD_ARROWS)) {
        ap2_led_sticky_set_key(3, 12, color);
        ap2_led_sticky_set_key(4, 10, color);
        ap2_led_sticky_set_key(4, 11, color);
        ap2_led_sticky_set_key(4, 12, color);
    } else {
        ap2_led_unset_sticky_key(3, 12);
        ap2_led_unset_sticky_key(4, 10);
        ap2_led_unset_sticky_key(4, 11);
        ap2_led_unset_sticky_key(4, 12);
    }

    switch (layer) {
    case _FN1_LAYER:
        rgb_matrix_sethsv_noeeprom(HSV_GREEN);
        break;
    case _FN2_LAYER:
        rgb_matrix_sethsv_noeeprom(HSV_BLUE);
        break;
    /*case _MOUSE1_LAYER:
    case _MOUSE2_LAYER:
        rgb_matrix_sethsv_noeeprom(HSV_WHITE);
        break;*/
    case _TG_LAYER:
        rgb_matrix_sethsv_noeeprom(HSV_RED);
        break;
    default:
        break;
    }

    // set_layer_indicator(layer);

    return state;
}

// Call when keypressed.
bool process_record_user(uint16_t keycode , keyrecord_t *record) {
    anne_pro_layers layer = get_highest_layer(layer_state);

    // Change splash color base on keycodes
    if (record->event.pressed && layer == _BASE_LAYER) {
        switch (keycode) {
            case KC_RGUI:
            case KC_LGUI:
                rgb_matrix_sethsv_noeeprom(0x8C, 0xFF, 0xFF);
                break;

            case KC_ESC:
            case QK_GESC:
            case KC_DEL:
            case KC_BSPC:
                rgb_matrix_sethsv_noeeprom(HSV_RED);
                break;

            case KC_ENT:
            case KC_PENT:
                rgb_matrix_sethsv_noeeprom(HSV_GREEN);
                break;

            default:
                rgb_matrix_sethsv_noeeprom(
                    0x8C, 0xFF, 0xFF
                );
                break;
        }
    }
    
    // switch (keycode) {
    //     case KC_RGUI:
    //     case KC_LGUI:
    //         super_key_pressed = record->event.pressed;
    //         // Reset layer indicator base on new super key state.
    //         set_layer_indicator(layer);
    //         break;

    //     // case KM_SLP:
    //     //     // Macro delay.
    //     //     if (record->event.pressed) {
    //     //         wait_ms(500);
    //     //     }
    //     //     return false;

    //     default:
    //         break;
    // }

    return true;
}

// The function to handle the caps lock logic
// It's called after the capslock changes state or after entering layers.
bool led_update_user(led_t leds) {
    if (leds.caps_lock) {
        // Set the caps-lock to red
        const ap2_led_t color = {
            .p.red   = 0xFF,
            .p.green = 0x00,
            .p.blue  = 0x00,
            .p.alpha = 0xFF
        };

        ap2_led_sticky_set_key(2, 0, color);
    } else {
        ap2_led_unset_sticky_key(2, 0);
    }

    return true;
}

// Macros
// void dynamic_macro_record_start_user(int8_t direction) {
//     const ap2_led_t color = {
//         .p.red   = 0xFF,
//         .p.green = 0x00,
//         .p.blue  = 0x00,
//         .p.alpha = 0xFF
//     };
// 
//     // Layer2 Q key is used to stop recoding.
//     // Highlight this key as visual indicator.
//     ap2_led_sticky_set_key(1, 1, color);
// }
// 
// void dynamic_macro_record_end_user(int8_t direction) {
//     // Layer2 Q key is used to stop recoding.
//     // Highlight this key as visual indicator.
//     ap2_led_unset_sticky_key(1, 1);
// }

